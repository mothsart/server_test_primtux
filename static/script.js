function launch(id) {
    var request = new XMLHttpRequest();
    request.open('POST', '/test', true);
    request.setRequestHeader(
        'Content-Type',
        'application/x-www-form-urlencoded; charset=UTF-8'
    );
    request.send('id=' + id);
    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            response = JSON.parse(request.response);
            line     = document.getElementById('test' + response.id);
            state    = line.getElementsByClassName('state')[0];
            errorE   = line.getElementsByClassName('error')[0];
            resultE  = line.getElementsByClassName('result')[0];
            input    = line.getElementsByTagName('input')[0];
            input.removeAttribute('disabled');
            errorE.classList.add('hidden');
            resultE.classList.add('hidden');
            aresultE = line.getElementsByClassName('attemptresult')[0];
            aresultE.innerHTML = response.attemptResult;
            if (response.state == 0) {
                state.classList.add('pass');
                state.innerText = 'Succès';
            } else if (response.state == 1) {
                state.classList.add('incorrect');
                state.innerText = 'Résultat != celui attendu';
                resultE.classList.remove('hidden');
                strong = resultE.getElementsByTagName('strong')[0];
                strong.innerHTML = response.result;
            } else if (response.state == 2) {
                state.classList.add('critical');
                state.innerText = 'Erreur';
                errorE.innerHTML = response.error;
                errorE.classList.remove('hidden');
            }
        } else {
            // We reached our target server, but it returned an error
        }
    };

    request.onerror = function() {
        console.log('Soucis du serveur Python !!');
    };
}

function launchAll() {
    launch(1);
}

function showHideDetails(element) {
    detailsE = element.parentNode.parentNode.getElementsByClassName('details')[0];
    if (detailsE.classList.contains('hidden')) {
        detailsE.classList.remove('hidden');
    } else {
        detailsE.classList.add('hidden');
    }
}
