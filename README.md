# Serveur de test de PrimTux

## Intro

Pour tester la distribution, rien de mieux qu'un petit serveur web taillé aux oignons.
C'est simple, adapté à l'usage et peut s'étoffer au fur et à mesure.

Un test peut prendre 3 états :

- succès : tout c'est déroulé comme attendu.
- incohérence : le résultat est différent de celui attendu.
- error : soucis critique ou un script c'est arrêté en cours, un logiciel a crashé etc.

## Lancer

L'argument représente le port que l'on souhaite lancer

[code]
python3 server_test_primtux.py 1111
[/code]
