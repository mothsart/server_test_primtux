#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# encoding=utf8

import sys
from enum import Enum
from subprocess import Popen, PIPE

import toml
from flask import (
    Flask, render_template, request, jsonify
)
app = Flask(__name__)

class State(Enum):
    SUCCESS   = 0
    INCORRECT = 1
    CRITICAL  = 2

TESTS = []

@app.route("/")
def index():
    if TESTS != []:
        return render_template(
            'index.html',
            name='index',
            TESTS=TESTS
        )
    parsed_toml = {} 
    with open('tests/dev.toml', 'r') as d:
        try:
            parsed_toml = toml.loads(d.read())
        except e:
            print(e)
    for test in parsed_toml:
        TESTS.append(parsed_toml[test])
    return render_template(
        'index.html',
        name='index',
        TESTS=TESTS
    )

@app.route("/test", methods=['POST'])
def test():
    _id   = request.form['id']
    test  = TESTS[int(_id) - 1]
    shell = test['shell'].split()
    state = State.SUCCESS
    with Popen(shell, stdout=PIPE, stderr=PIPE) as proc:
        error  = proc.stderr.read().decode('utf8')
        result = proc.stdout.read().decode('utf8')
        if test['result'] != result:
            state = State.INCORRECT
        if error:
            state = State.CRITICAL
        return jsonify({
            'id':             _id,
            'state':          int(state.value),
            'attemptResult':  str(test['result']),
            'result':         str(result),
            'error':          str(error)
        })

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Nécessite de communiquer le port HTTP en argument')
        exit(0)
    port = sys.argv[1]
    if not port.isdigit() or len(port) != 4:
        print('Le port HTTP doit être un entier de 4 chiffres')
        exit(0)
    app.run(host='127.0.0.1', port=port)
